module gitlab.com/RandomCivil/proxy-router

go 1.15

require (
	github.com/oschwald/geoip2-golang v1.4.0
	gitlab.com/RandomCivil/prefix-search v0.0.0-20210801083511-50500e03c888
)
