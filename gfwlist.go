package proxy_router

import (
	"bufio"
	"bytes"
	"encoding/base64"
	"os"
	"strings"

	"gitlab.com/RandomCivil/prefix-search/trie"
)

const bufSize = 5 * 1024

type GfwList struct {
	f          *os.File
	domains    []string
	prefixTrie *trie.Node
	suffixTrie *trie.Node
}

func NewGfwList(path string) *GfwList {
	f, err := os.OpenFile(path, os.O_RDONLY, 0755)
	if err != nil {
		panic(err)
	}
	return &GfwList{f: f, prefixTrie: trie.New(), suffixTrie: trie.New()}
}

func (g *GfwList) Has(domain string) bool {
	var result []string
	result = g.prefixTrie.FindByPrefix(domain)
	if len(result) > 0 {
		return true
	}

	domainSlice := strings.Split(domain, ".")
	var suffixDomain string
	if len(domainSlice) > 2 {
		suffixDomain = strings.Join(domainSlice[len(domainSlice)-2:], ".")
	} else {
		suffixDomain = domain
	}
	rsuffixDomain := reverse(suffixDomain)
	result = g.suffixTrie.FindByPrefix(rsuffixDomain)
	if len(result) > 0 {
		return true
	}

	for _, d := range g.domains {
		if strings.Contains(domain, d) {
			return true
		}
	}
	return false
}

func (g *GfwList) ParseGfwList() {
	defer g.f.Close()

	bb := bytes.NewBuffer(nil)
	wt := bufio.NewWriter(bb)
	buf := make([]byte, bufSize)
	for {
		n, err := g.f.Read(buf)
		if err != nil {
			break
		}
		wt.Write(buf[:n])
	}
	wt.Flush()

	data, err := base64.StdEncoding.DecodeString(bb.String())
	if err != nil {
		panic(err)
	}

	scanner := bufio.NewScanner(bytes.NewReader(data))
	for scanner.Scan() {
		line := strings.TrimSpace(scanner.Text())
		if !strings.ContainsRune(line, '.') {
			continue
		}
		domain := line
		switch line[0] {
		case '.':
			domain := line[1:]
			g.domains = append(g.domains, domain)
		case '|':
			if line[1] == '|' {
				domain = domain[2:]
				if strings.ContainsRune(domain, '*') {
					continue
				}
				rs := reverse(domain)
				g.suffixTrie.Generate([]byte(rs))
			} else {
				domain = domain[1:]
				if strings.ContainsRune(domain, '*') {
					continue
				}
				g.prefixTrie.Generate([]byte(domain))
			}
		}
	}
}

func reverse(s string) string {
	runes := []rune(s)
	for i, j := 0, len(runes)-1; i < j; i, j = i+1, j-1 {
		runes[i], runes[j] = runes[j], runes[i]
	}
	return string(runes)
}
