package proxy_router

import (
	"net"

	"github.com/oschwald/geoip2-golang"
)

type GeoIP2 struct {
	db *geoip2.Reader
}

func NewGeoIP2(path string) *GeoIP2 {
	db, err := geoip2.Open(path)
	if err != nil {
		panic(err)
	}
	return &GeoIP2{db: db}
}

func (g *GeoIP2) Close() {
	g.db.Close()
}

func (g *GeoIP2) ParseGeoip2(ipp string) string {
	// If you are using strings that may be invalid, check that ip is not nil
	ip := net.ParseIP(ipp)
	record, err := g.db.City(ip)
	if err != nil {
		panic(err)
	}
	return record.Country.IsoCode
}
